import stories
import time
import conversation_content as content
import story_content
import random
import signal
import re
import memory
import irc
import threading

NAME = None
THREAD = None
PARTNER = None
GREETING = None
TIMER = None
TIMEOUTS = 0
FUTURES = []

def resetTimer(c, channel, src):
    global TIMER

    if (TIMER):
        TIMER.cancel()
    TIMER = threading.Timer(60, takeInput, args=['', True, c, channel, src])
    TIMER.start()
    return

def subName(text):
    global NAME
    if NAME == None:
        found = re.findall('\[.*?\]', text)
        for f in found:
            text = text.replace(f, '')
    else:
        found = re.findall('\[.*?\]', text)
        for f in found:
            replaced = f.replace('[', '')
            replaced = replaced.replace(']', '')
            replaced = replaced.replace('$', NAME)
            text = text.replace(f, replaced)
    return text

def findName(text):
    global NAME
    for phrase in ['my name is', "my name's", "my names"]:
        if phrase in text.lower():
            text = text.lower().replace(phrase, '').strip()
            NAME = text.split()[0].title()
            return NAME

# change for IRC
def say(sentence, c=None, channel=None):
    sentence = subName(sentence)
    if c != None and channel != None:
        print('sending IRC message...')
        c.privmsg(channel, sentence)
    else:
        print('\t>', sentence)

# change if needed
def wait(seconds):
    time.sleep(seconds)

def checkKeywords(inp):
    for key in content.KEYWORDS:
        for k in key:
            if k.lower() in inp.lower():
                return random.choice(content.KEYWORDS[key])
    return None

def getInteruptResponse():
    return random.choice(stories.INTERRUPTED)

def handle(out, c, channel):
    if type(out) != list:
        out = [out]
    for s in out:
        if type(s) == int:
            wait(s)
        else:
            wait(s[0])
            say(s[1], c, channel)

def isBusy():
    global THREAD
    if THREAD == None or not THREAD.isAlive():
        THREAD = None
        return False
    return True

def storyTime(c, channel, asked=False, text=None):
    global THREAD
    THREAD = threading.Thread(target=handle, args=(stories.getStory(asked, text), c, channel))
    THREAD.start()
    #thread.start_new_thread(handle, (stories.getStory(asked, text), c, channel))
    #handle(stories.getStory(asked, text), c, channel)

def handleResponse(inp, c, channel):
    out = checkKeywords(inp)
    if out == None:
        if random.choice(range(0, 5)) == 0:
            #handle(stories.getStory(), c, channel)
            storyTime(c, channel)
        else:
            handle((1, random.choice([
                "I'm confused, what?",
                "I don't understand, what was your question?",
                "[$, ]I have no idea what you're talking about.",
                "[$, ]I don't know why I should care about that.",
                "I don't care[, $].",
                "Don't bother me about stupid crap[, $].",
                "Why should I care?",
                "Young people these days care about the dumbest stuff."
            ])), c, channel)
    elif out == content.TELL_STORY:
        #handle(stories.getStory(), c, channel)
        storyTime(c, channel)
    elif out == content.TELL_STORY_ASK:
        #handle(stories.getStory(True), c, channel)
        storyTime(c, channel, True)
    elif out == content.GET_NAME:
        findName(inp)
        handle((1, random.choice([
            "Nice to meet you[, $].",
            "Hey[ $].",
            "Howdy[ $], nice to meet you."
        ])), c, channel)
    else:
        handle((stories.getDelay(out), out), c, channel)

def forget():
    global NAME
    global THREAD
    global PARTNER
    global FUTURES
    NAME = None
    THREAD = None
    PARTNER = None
    FUTURES = []
    memory.forget()

def isEnding(inp):
    for phrase in ("bye", "see you", "meet you", "talking to you", "meeting you", "gotta go"):
        if phrase in inp.lower():
            return True
    return False

def takeInput(inp='', timeout=True, c=None, channel=None, src=None):
    global NAME
    global PARTNER
    global GREETING
    global TIMER
    global TIMEOUTS

    if (not timeout):
        resetTimer(c, channel, src)
        TIMEOUTS = 0
    else:
        TIMEOUTS += 1

    resetTimer(c, channel, src)

    #Non greeting timeout
    if (timeout and PARTNER and not GREETING):
        if (TIMEOUTS == 1):
            handle((1, random.choice(['Young people these days have no manners', "Hello? Anyone there?"])), c, channel)
        else:
            handle((1, random.choice(["Ok, forget you.", 'Back in my day we had a little thing called manners!'])), c, channel)
            PARTNER = None  #Stop talking to current partner
            TIMEOUTS = 0    #Reset Timeouts
            TIMER = None
            if len(FUTURES) > 0:#continue onto next partner in future list if one exists
                new = FUTURES[0]
                del FUTURES[0]
                PARTNER = new
                handle((1, "Alright, " + new + ", I can talk now."), c, channel)
                GREETING.reset()
        return

    #greeting timeout
    elif (timeout and PARTNER and GREETING):
        GREETING.timeoutResponse()
        say(GREETING.respond(False), c, channel)
        resetTimer(c, channel, src)
        if (GREETING.state[2:] == 'GIVEUP_FRUSTRATED' and not GREETING.goodExit):
            PARTNER = None #Stop talking to current partner
            TIMEOUTS = 0
            TIMER = None
            GREETING = None #Out of greeting mode
            if len(FUTURES) > 0:#continue onto next partner in future list if one exists
                new = FUTURES[0]
                del FUTURES[0]
                PARTNER = new
                handle((1, "Alright, " + new + ", I can talk now."), c, channel)
                GREETING.reset()
        return

    # name from nickname
    if src != None:
        if PARTNER == None:
            PARTNER = src
            #Conversation partner change
            GREETING = GreetingBot()
            GREETING.react(inp)
            prev = None
            for i in range(3):
                if (GREETING.botsTurn() and prev != GREETING.state):
                    prev = GREETING.state
                    say(GREETING.respond(True), c, channel)
            if (GREETING.state == 'END'):
                if (not GREETING.goodExit):
                    PARTNER = None #Stop talking to current partner
                    TIMEOUTS = 0
                    TIMER = None
                    if len(FUTURES) > 0:#continue onto next partner in future list if one exists
                        new = FUTURES[0]
                        del FUTURES[0]
                        PARTNER = new
                        handle((1, "Alright, " + new + ", I can talk now."), c, channel)
                        GREETING.reset()
                        return
                else:
                    resetTimer(c, channel, src)
                    print('good greeting exit')
                    handle((1, "That's good to hear[, $]."), c, channel)
                GREETING = None #Out of greeting mode
            return
        elif PARTNER != src:
            # add future conversation partner
            n = NAME
            if n == None:
                n = PARTNER
            handle((1, "Hold on " + src + ", I'm talking with " + n + '.'), c, channel)
            if src not in FUTURES:
                FUTURES.append(src)
            return         
        if NAME == None:
            #If a name wasn't given, use irc handle
            NAME = src
    
    # handle being busy
    if isBusy():
        handle((1, random.choice(story_content.INTERRUPTED)), c, channel)
    elif (GREETING):
        GREETING.react(inp)
        prev = None
        for i in range(3):
            if (GREETING.botsTurn() and prev != GREETING.state):
                prev = GREETING.state
                say(GREETING.respond(True), c, channel)
        if (GREETING.pos == 1 and GREETING.state == '1_INQUIRY_REPLY'):
            GREETING.state = 'END'
            GREETING.goodExit = True
        if (GREETING.state == 'END'):
            if (not GREETING.goodExit):
                PARTNER = None #Stop talking to current partner
                TIMEOUTS = 0
                TIMER = None     
                if len(FUTURES) > 0:#continue onto next partner in future list if one exists
                    new = FUTURES[0]
                    del FUTURES[0]
                    PARTNER = new
                    handle((1, "Alright, " + new + ", I can talk now."), c, channel)
                    GREETING.reset()
                    return
            else:
                print('good greeting exit')
                handle((1, "That's good to hear[, $]."), c, channel)
            GREETING = None #Out of greeting mode
        return
    else:
        r = memory.recall(inp)
        remembered = memory.remember(inp)
        with open('log.txt', 'a') as f:
            f.write(inp + '\n')
        if r != None:
            # answer questions involving memory
            handle((stories.getDelay(r), r), c, channel)
        else:
            if remembered:
                handle((1, random.choice(content.NEW_FACT)), c, channel)
            else:
                sentences = stories.findStory(inp)
                if sentences != None:
                    # remember a story based on input
                    #handle(stories.getStory(False, sentences), c, channel)
                    storyTime(c, channel, False, sentences)
                else:
                    # handle normal response
                    handleResponse(inp, c, channel)
                    if isEnding(inp):
                        PARTNER = None
                        #TODO - may need to remove to make greeting work properly 
                        if len(FUTURES) > 0:
                            new = FUTURES[0]
                            del FUTURES[0]
                            PARTNER = new
                            handle((1, "Alright, " + new + ", I can talk now."), c, channel)

class GreetingBot:

    def __init__(self):
        self.pos = 2
        other = 2 if self.pos == 1 else 1
        self.state = '1_INITIAL_OUTREACH' if self.pos == 1 else 'START'
        self.goodExit = False
        self.utterances = {}
        self.utterances['1_INITIAL_OUTREACH'] = ['hi', 'hello', 'howdy', 'good morning',
            'good afternoon', 'morning', 'afternoon'] 
        self.utterances['1_SECONDARY_OUTREACH'] = ['i said hi', 'excuse me, hello?']
        self.utterances['1_GIVEUP_FRUSTRATED'] = ['Ok, forget you.', 'whatever.']
        self.utterances['1_INQUIRY'] = ['how are you?', 'what\'s happening?', "how's it going?"]
        self.utterances['1_INQUIRY_REPLY'] = ['i\'m good', 'i\'m fine', 'fine', 'alright',
            'been better', "I'm alright", "I'm good enough", 'good enough']
        self.utterances['2_OUTREACH_REPLY'] = ['hi', 'howdy', 'Good afternoon.', 'hello']
        self.utterances['2_INQUIRY'] = ['how about you?', 'and yourself?']
        self.utterances['2_GIVEUP_FRUSTRATED'] = ['ok, forget you.', 'whatever.']
        self.utterances['2_INQUIRY_REPLY'] = ['i\'m good', 'i\'m fine', 'fine', 'alright',
            'fine enough', "I'm alright", "I'm good enough", 'good enough']
        
        #Current to next state mappings, does not include timeout dependent state transitions
        #Make sure all states that point to 'END', don't point to any other states (others will be ignored)
        self.nextState = {}
        self.nextState['1_INITIAL_OUTREACH'] = ['2_OUTREACH_REPLY']
        self.nextState['1_SECONDARY_OUTREACH'] = ['2_OUTREACH_REPLY']
        self.nextState['1_INQUIRY'] = ['2_INQUIRY_REPLY']
        self.nextState['START'] = ['1_INITIAL_OUTREACH']
        self.nextState['2_OUTREACH_REPLY'] = ['1_INQUIRY']
        self.nextState['2_INQUIRY_REPLY'] = ['2_INQUIRY']
        self.nextState['2_INQUIRY'] = ['1_INQUIRY_REPLY']
        self.nextState['1_GIVEUP_FRUSTRATED'] = ['END'] #improper end of conversation
        self.nextState['2_GIVEUP_FRUSTRATED'] = ['END'] #improper end of conversation
        self.nextState['UNEXPECTED'] = ['END']          #improper end of conversation
        self.nextState['UNKNOWN'] = ['END']          #improper end of conversation
        self.nextState['1_INQUIRY_REPLY'] = ['END']     #proper end of conversation
        
        self.send = {}
        self.receive = {}
        for key, value in self.utterances.items():
            if (key[0] == str(self.pos)):
                self.send[key] = value
            elif (key[0] == str(other)):
                self.receive[key] = value
        self.send['UNEXPECTED'] = ['What?']
        self.send['UNKNOWN'] = ['You\'re not making any sense', 'What?']
        return

    def reset(self):
        self.state = '1_INITIAL_OUTREACH' if self.pos == 1 else 'START'
        return

    #returns next thing and advances state if next state belongs to bot
    def respond(self, advance):
        current = self.state
        if (advance):
            nextOne = random.choice(self.nextState[current])
            self.state = nextOne if nextOne in self.send else current
        if (current == 'START' or current == 'END'):
            return ''
        else:
            return random.choice(self.send[current])
    
    def botsTurn(self):
        return self.state in self.send

    #takes next thing and advances state
    def react(self, text):
        current = self.state
        print(current)
        if ('END' in self.nextState[current]):
            self.state = 'END'  #end of greeting/conversation
        else:
            textClass = self.classifyResponse(text)
            print(textClass)
            if (textClass == 'TIMEOUT'): #conversation response timeout
                print('in react')
                if (current == '1_INITIAL_OUTREACH'):
                    self.state = '1_SECONDARY_OUTREACH'
                elif (current == '1_SECONDARY_OUTREACH'):
                    self.state = '1_GIVEUP_FRUSTRATED'
                elif (current == '1_INQUIRY'):
                    self.state = '1_GIVEUP_FRUSTRATED'
                elif (current == '2_OUTREACH_REPLY'):
                    self.state = '2_GIVEUP_FRUSTRATED'
                elif (current == '2_INQUIRY'):
                    self.state = '2_GIVEUP_FRUSTRATED'
                elif (current == 'START'):
                    self.state = 'START'
                else:
                    self.state = 'END'
            elif (textClass not in self.nextState[current] and textClass != current):
                self.state = 'UNEXPECTED' #not a valid jump from previous state
            elif (textClass == 'UNKNOWN'):
                self.state = 'END'
            else:
                self.state = random.choice(self.nextState[textClass]) #advance state (normal case)
                if (textClass == '1_INQUIRY_REPLY'):
                    self.goodExit = True
        print(self.state)
        return

    def timeoutResponse(self):
        current = self.state
        if (current == '1_INITIAL_OUTREACH'):
            self.state = '1_SECONDARY_OUTREACH'
        elif (current == '1_SECONDARY_OUTREACH'):
            self.state = '1_GIVEUP_FRUSTRATED'
        elif (current == '1_INQUIRY'):
            self.state = '1_GIVEUP_FRUSTRATED'
        elif (current == '2_OUTREACH_REPLY'):
            self.state = '2_GIVEUP_FRUSTRATED'
        elif (current == '2_INQUIRY'):
            self.state = '2_GIVEUP_FRUSTRATED'
        elif (current == 'START'):
            self.state = 'START'
        else:
            self.state = 'END'
        return

    def classifyResponse(self, resp):
        textClass = 'UNKNOWN'
        if (resp):
            for category in self.receive:
                if (resp.lower() in self.receive[category]):
                    textClass = category
        else:
            textClass = 'TIMEOUT'
        return textClass

if __name__ == '__main__':
    while True:
        inp = input('$ ')
        takeInput(inp)          
            
            
