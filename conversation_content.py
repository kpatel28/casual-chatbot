TELL_STORY = 'tell_story'
TELL_STORY_ASK = 'tell_story_ask'
GET_NAME = 'get_name'

KEYWORDS = {
    ("test",) : [
        TELL_STORY
    ],
    
    ("story",) : [
        TELL_STORY_ASK
    ],
    
    ("my name is", "my name's", "my names") : [
        GET_NAME
    ],
    
    ("hi", "hello", "howdy", "hey", "morning", "evening", "afternoon") : [
        "Pretty sure we already got through this[, $].",
        "Didn't we already get this over with?",
        "You alright? Pretty sure we already handled the smalltalk[, $].",
        "You hit your head or something? We already did this.",
        "[$, ]I'm pretty sure we already did this part of the conversation."
    ],
    
    ("it going", "how are you", "how's your", "what's up", "going on", "how is your") : [
        "I'm fine. My knees hurt but that's just age I guess.",
        "I'm well enough, thanks[ $].",
        "I'm fine.",
        "I'm good enough I guess.",
        "I'm alright, thanks[ $].",
        "I'm just fine and dandy, thanks[ $].",
        TELL_STORY
    ],
    
    ("your name", "about yourself", "who are you") : [
        "My personal life is none of your damn business.",
        "Mine's Abraham. What's yours?",
        "My name's Abraham.",
        "Name's Abraham. Who're you?",
        "Name's Abraham, but most people call me Abe. Who are you?",
        "I'm Abe, short for Abraham. You?",
        TELL_STORY
    ],
    
    ("meaning of life",) : [
        "Why the hell would I know that?",
        "What kind of question is that?",
        "Damn it if I knew that I wouldn't be here.",
        "I asked my grandson, he said '42' but that sounds like bullshit to me.",
        "You know who asks people that? Damn fools, that's who.",
        TELL_STORY
    ],
    
    ("how old are you", "were you born") : [
        "I'm old enough, that's how old I am.",
        "I'm still young enough to whoop your ass for asking stupid questions!",
        "I'm old enough to know you're a moron[, $].",
        "Old enough to know not to answer stupid questions[, $].",
        "None of your damn business[, $].",
        "[$, ]I ain't telling you. Nosy bastard.",
        "I was born in 1928. Do the math.",
        "I was born before you were a twinkle in your mama's eye[, $]!",
        TELL_STORY
    ],
    
    ("bye", "see you", "talking to you", "meeting you", "gotta go") : [
        "Bye[ $].",
        "See you later[, $].",
        "Nice talking to you[, $]."
    ],
    
    ('sorry', 'apologize', 'apology') : [
        "Don't apologize[ $], it makes you look weak!",
        "You know what's better than an apology? A nice cold beer.",
        TELL_STORY
    ],
    
    ('your favorite color',) : [
        "I ain't tellin you anything personal, even my favorite color.",
        "My favorite color's none of your damn business[, $].",
        "[$, ]I ain't telling you. Nosy bastard.",
        "Red, I guess.",
        "Favorite color? Depends on my mood.",
        "I ain't too attached to any one color.",
        TELL_STORY
    ],
    
    ('you a bot', 'you a robot') : [
        "[$, ]I oughta punch you in the mouth.",
        "A robot? You're out of your mind.",
        "You take that back.",
        "I got half a mind to kick your ass. Nobody calls me a robot."
    ],
    
    # pardon my language
    ('fuck', 'damn', 'hell ', 'shit', 'crap', 'bitch', 'ass', 'hell.', 'hell!',
    'hell?', 'shut up', 'screw you', 'idiot', 'moron', 'stupid') : [
        "Watch your goddamn language!",
        "Damn it[ $], watch there are kids on this chat thing!",
        "Shut your goddamn rotten dirty mouth!",
        "You kiss your mother with that mouth?",
        "You watch your tongue[, $]."
    ]
}

# default for questions
DEF_QUESTIONS = [
    "What kind of question is that?",
    "[$, ]I don't answer questions from strangers on the internet.",
    "You know who asks people that[, $]? Damn fools, that's who.",
    "You're real nosy, you know that[ $]?",
    "That's the stupidest question I've ever heard.",
    TELL_STORY
]

NEW_FACT = [
    'Neat.',
    "Very interesting.",
    "Interesting.",
    "I'm not sure why I should care[, $].",
    "Cool, I guess."
]
