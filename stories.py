import story_content as content
import random
import time

FORGET_CHANCE = 20
CAPS_CHANCE = 30
SEC_RANGE = (.25, .5)

PUNC = '.,?!"' + "'"

def getDelay(sentence):
    return max(1, int(len(sentence.split()) * random.uniform(SEC_RANGE[0], SEC_RANGE[1])))

def findStory(inp):
    options = []
    for word in inp.lower().split():
        for p in PUNC:
            word = word.replace(p, '')
        for keys in content.STORIES:
            if word in keys:
                options.append(keys)
    if len(options) == 0:
        return None
    else:
        return getSentences(content.STORIES[random.choice(options)].split('.'))

def getSentences(story=None):
    if story == None:
        choice = random.choice(list(content.STORIES.keys()))
        story = content.STORIES[choice].split('.')
        
    story = [s.strip() for s in story if len(s.strip()) > 0]
    sents = []
    for s in story:
        s = s.strip() + '.'
        parts = s.split('?')
        for p in parts:
            if p[-1] != '.':
                sents.append(p.strip() + '?')
            else:
                sents.append(p.strip())
    return sents

def getCaps(sentence):
    # planned sentence
    story = [(getDelay(sentence), sentence.upper())]
    # reaction to caps lock
    caps = random.choice(content.CAPS) + ' ' + random.choice(content.CAPS_RETURNS)
    story.append((getDelay(caps), caps))
    # extra delay to collect thoughts
    story.append(3)
    return story

# call this to get a story (a list of tuples with a delays and a sentence)
def getStory(asked=False, text=None):    
    # story intro
    if not asked:
        intro = random.choice(content.INTROS)
        story = [(getDelay(intro), intro)]
    else:
        story = []

    # story content
    if text == None:
        text = getSentences()
    forgot = False
    justCaps = False # dont hit caps twice in a row
    for sent in text:
        if random.choice(range(0, FORGET_CHANCE)) == 0:
            # forget what he was saying
            forg = random.choice(content.FORGOT)
            story.append((getDelay(forg), forg))
            forgot = True
            break
        else:
            # continues with story
            if not justCaps and random.choice(range(0, CAPS_CHANCE)) == 0:
                # accidentally hits caps lock
                story.extend(getCaps(sent))
                justCaps = True
            else:
                # continues normally
                if justCaps:
                    story.append((getDelay(sent), random.choice(content.REMEMBER) + ' ' + sent[0].lower() + sent[1:]))
                else:
                    story.append((getDelay(sent), sent))
                justCaps = False
            
    # story ending
    if not forgot:
        end = random.choice(content.ENDINGS)
        story.append((getDelay(end), end))
        
    return story
        
        
