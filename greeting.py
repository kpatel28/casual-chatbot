#! /usr/bin/env python
#
import random, signal, time

class timeout:
    def __init__(self, seconds=1, error_message='Timeout'):
        self.seconds = seconds
        self.error_message = error_message
 
    def handle_timeout(self, signum, frame):
        raise TimeoutError(self.error_message)
 
    def __enter__(self):
        signal.signal(signal.SIGALRM, self.handle_timeout)
        signal.alarm(self.seconds)

    def __exit__(self, type, value, traceback):
        signal.alarm(0)

class GreetingBot:

    def __init__(self):
        self.pos = 2
        other = 2 if self.pos == 1 else 1
        self.state = '1_INITIAL_OUTREACH' if self.pos == 1 else 'START'
        self.goodExit = False
        self.utterances = {}
        self.utterances['1_INITIAL_OUTREACH'] = ['hi', 'hello'] 
        self.utterances['1_SECONDARY_OUTREACH'] = ['I said hi', 'excuse me, hello?']
        self.utterances['1_GIVEUP_FRUSTRATED'] = ['Ok, forget you.', 'Whatever.']
        self.utterances['1_INQUIRY'] = ['how are you?', 'what\'s happening?']
        self.utterances['1_INQUIRY_REPLY'] = ['I\'m good', 'I\'m fine, thanks for asking']
        self.utterances['2_OUTREACH_REPLY'] = ['hi', 'hello back at you!']
        self.utterances['2_INQUIRY'] = ['how about you?', 'and yourself?']
        self.utterances['2_GIVEUP_FRUSTRATED'] = ['Ok, forget you.', 'Whatever.']
        self.utterances['2_INQUIRY_REPLY'] = ['I\'m good', 'I\'m fine']
        
        #Current to next state mappings, does not include timeout dependent state transitions
        #Make sure all states that point to 'END', don't point to any other states (others will be ignored)
        self.nextState = {}
        self.nextState['1_INITIAL_OUTREACH'] = ['2_OUTREACH_REPLY']
        self.nextState['1_SECONDARY_OUTREACH'] = ['2_OUTREACH_REPLY']
        self.nextState['1_INQUIRY'] = ['2_INQUIRY_REPLY']
        self.nextState['START'] = ['1_INITIAL_OUTREACH']
        self.nextState['2_OUTREACH_REPLY'] = ['1_INQUIRY']
        self.nextState['2_INQUIRY_REPLY'] = ['2_INQUIRY']
        self.nextState['2_INQUIRY'] = ['1_INQUIRY_REPLY']
        self.nextState['1_GIVEUP_FRUSTRATED'] = ['END'] #improper end of conversation
        self.nextState['2_GIVEUP_FRUSTRATED'] = ['END'] #improper end of conversation
        self.nextState['1_INQUIRY_REPLY'] = ['END']     #proper end of conversation
        
        self.send = {key: self.utterances[key] for key in self.utterances if key[0] == str(self.pos)}
        self.receive = {key: self.utterances[key] for key in self.utterances if key[0] == str(other)}
        return

    def reset(self):
        self.state = '1_INITIAL_OUTREACH' if self.pos == 1 else 'START'
        return

    def respond(self):
        return random.choice(self.send[self.state])
    
    def botsTurn(self):
        return self.state in self.send

    #State machine to get next state, blocks for input if appropriate
    def awaitResponse(self):
        current = self.state

        if ('END' in self.nextState[current]):
            self.state = 'END'  #end of greeting/conversation
            if (current == '1_INQUIRY_REPLY'):
                self.goodExit = True
        else:
            dontWait = [(state in self.nextState[current]) for state in self.send]
            if (any(dontWait)):
                #if there's no need for input, next state is based on the last thing said
                self.state = random.choice(self.nextState[current])
            else:
                #get input
                timeout = 3 if current != 'START' else 180
                text = self.getResponse(timeout)
                textClass = self.classifyResponse(text)
                self.state = textClass #normal case
                
                #abnormal cases
                if (textClass == 'UNKNOWN'):
                    self.state = 'END'
                elif (textClass == 'TIMEOUT'): #conversation response timeout
                    if (current == '1_INITIAL_OUTREACH'):
                        self.state = '1_SECONDARY_OUTREACH'
                    elif (current == '1_SECONDARY_OUTREACH'):
                        self.state = '1_GIVEUP_FRUSTRATED'
                    elif (current == '1_INQUIRY'):
                        self.state = '1_GIVEUP_FRUSTRATED'
                    elif (current == '2_OUTREACH_REPLY'):
                        self.state = '2_GIVEUP_FRUSTRATED'
                    elif (current == '2_INQUIRY'):
                        self.state = '2_GIVEUP_FRUSTRATED'
                    elif (current == 'START'):
                        self.state = 'END'
                    else:
                        self.state = 'END'
        return
        
    def getResponse(self, timeoutSec):
        text = ''
        try:
            with timeout(seconds=timeoutSec):
                text = input('') #TODO - use irc bot input here
        except TimeoutError:
            text = ''
        finally:
            return text

    def classifyResponse(self, resp):
        textClass = 'UNKNOWN'
        if (resp):
            for category in self.receive:
                if (resp in self.receive[category]):
                    textClass = category
        else:
            textClass = 'TIMEOUT'
        return textClass

def runGreeting():
    
    g = GreetingBot()
    
    while (g.state != 'END'):
        if (g.botsTurn()):
            print(g.respond())
        g.awaitResponse()

    return g.goodExit
ret = runGreeting()
#print(ret)