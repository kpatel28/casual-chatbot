Team:
    Gavin Scott
    Kevin Patel

To run out bot, run "python3 ircbot.py" with an optional parameter specifying
the IRC channel. Without the parameter, it will default to the class IRC channel.

Things that can trigger interesting actions once you're past the greeting:

    - Attempting to message it while it is in a conversation with someone else
        - Ending the current conversation ('goodbye') will cause it to start up with
          the other person
    - Tell it things about youself ("my favorite color is orange") and ask it to
      recall them ("What did I say my favorite color was?")
    - Tell it your name ("My name is Jim"). It will start using your name throughout
      its normal responses
    - Ask it to tell you a story
        - Stories will sometimes be forgotten midway through, or the bot will
          "accidentally" turn on caps lock and get frustrated about it
        - Stories can also just randomly start
    - "Remind" the bot of an old story by mentioning certain keywords (some
      overlap, so here are some unique ones):
        - cow, mayo, elmo, bull, typewriter, jail
    - Say something mean to the bot (profanity is encouraged)
    - Don't respond to the bot for enough time and it will complain about you

