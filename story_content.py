STORIES = {
    (
        'war', 'navy', 'cow', 'fight', 'ship', 'private', 'ww1', 'wwI', 'brawl',
        'ww2', 'wwII', 'vietname', 'asia', 'ocean', 'sea', 'island', 'beach',
        'water', 'battle', 'combat', 'battles', 'wars'
    ) : "This was back in the war, back when I was a private in the navy. We'd been floatin around for weeks with nothin to do and we were gettin real ornery. Fights would break out damn near every night, folks would go at eachother for pretty much nothing. The brass were getting tired of it, so one day they called us all out on deck for practice. We all ran out to battle stations and waited for the order. We were floating by a little island covered in grass, no people anywhere near the water. Nothing much was near the water really, except for this one cow, one of them wild cows that wandered around eating that nasty grass by the waterside. Anyway[ $ ,] we were waiting for the order, then it came over the intercom like this, I remember it clear as day. 'Men, sink the cow'. Every damn gun on ship spun around to that little island and I swear when we were done there wasn't a hair left of that cow, hell there wasn't much left of the whole damn beach.",

    (
        'war', 'navy', 'hair', 'fight', 'ship', 'private', 'ww1', 'wwI', 'brawl',
        'ww2', 'wwII', 'vietname', 'asia', 'ocean', 'sea', 'island', 'beach',
        'water', 'battle', 'combat', 'sandwich', 'kitchen', 'clean', 'mayo',
        'gel', 'slick', 'battles', 'wars'
    ) : "You know that fancy goop people put in there hair nowadays[, $]? I'll never understand why there's so many damn choices. Back in my day we had pomade and that was it. Except back in the war we'd be out floating around so long we'd run out. And if you showed me a man that said he bought pomade in one of them villages I'd show you a liar. But back then if you're hair weren't slicked back proper you'd never hear the end of it. You know what we used instead[, $]? Mayo. Scraped the damn mayo out our sandwiches and slicked it through our hair. Worked fine except when you stood out in the sun too long and it started to turn. Brass clamped down on that soon as they found out, made us clean the kitchen floor damn near all night.",
    
    (
        'father', 'dad', 'elmo', 'rufus', 'grandpa', 'grandfather', 'onion',
        'york', 'farm', 'parent', 'parents', 'family'
    ) : "I ever told you about my father? His name was Elmo Rufus. Elmo Rufus. What my grandad was thinkin when they named him I'll never know. Talking about grandad reminds me of another story actually but I'll tell you later. Anyways my grandad wasn't making much money, so when pops was nine they sent him off to an onion farm. Farmers put him up for a year or so 'fore he went home. Only, when he got home, nobody else was there. Grandparents had up and left without tellin him[, $]. I still can't believe it. Mind you he was a ten year old boy in New York. He went back to the farm for a couple more years, didn't see his family again for twenty-odd years. Bumped into his dad in New York city years later. I'd have punched him in the mouth myself, but ol' dad was nicer than me.",
    
    (
        'bull', 'bar', 'george', 'grandpa', 'grandfather', 'die', 'dead', 'death',
        'york', 'farm', 'grandparent', 'grandparents', 'family'
    ) : "I've told you about my grandad George right? Ditched his kid at an onion farm? Well if I haven't told you that one yet I'll get to it later. You know how most people died back in my day[, $]? Disease maybe. War sometimes too. Not ol' George. My grandad died coming out of a bar. Not a bar fight like your thinking. Not one of them D U I things that're in the news nowadays neither. Nope, George got run down by a bull on his way out a bar. Grandpa George was the one coming out the bar, not the bull, in case you were confused. A goddamn bull.",
    
    (
        'son', 'sons', 'daughter', 'daughters', 'wife', 'children', 'family',
        'motorcycle', 'business', 'work', 'living', 'job', 'typewriter', 'parent',
        'parenting', 'kids', 'kid'
    ) : "Now I got four sons, no daughters. Wife wasn't too happy about that, believe me[ $]. But one of 'em stands out a bit, not really for good reasons. He's a trouble maker, always was. Gets in fights all the time, never got married and had kids, that kind of thing. Bought a motorcycle when he was in highschool and I swear I almost kicked his ass right then and there. Back then I worked for a typewriter company. I was important too, had lots of connections. This was before computers, back when people still understood that a good typewriter was a nice thing. Anyway I had a bigwig in town and I was showing him around town. We were driving around, when all of a sudden I get on the freeway and guess who drives past me. My dumbass son shootin down the road on his motorcycle. Except he wasn't sittin on it proper. Oh no. He was standin on it like a damn surfboard. I near shit myself[, $]. The bigwig said something like 'Damn crazy kid' or something like that and I pretended I didn't know him. You better believe he heard about that when I got home. Kids are all fools I say, damn fools.",
    
    (
        'frank', 'friend', 'friends', 'buddy', 'buddies', 'war', 'jail', 'dui',
        'driving', 'prison', 'beverly', 'marriage', 'wife', 'family', 'drunk',
        'drinking', 'alcohol', 'women', 'girlfriend', 'woman', 'girlfriends',
        'relationship', 'relationships', 'dating', 'married'
    ) : "I used to have a buddy named Frank. Good buddy, knew him in the war and stayed close after. Anyway he was seein this pretty broad named Beverly. The three of us were thick as thieves. They were together for a while, then Frank went and proposed. Only then a month or so later he was fool enough to go get drunk, hop in his car and run some poor fella down. Judge sent him to jail straightaway. Needless to say, Beverly was all broken up about it[, $], and I wasn't happy either. He told me to look after his fiance for him, and I did. We got married a few years later. I don't talk to Frank much anymore, poor guy."
}

INTROS = [
    "Hold on a minute[ $], that actually reminds me of a story.",
    "Actually[, $,] now that you mention that I just remembered something.",
    "Hold that thought[ $], I just remembered something.",
    "Hold on[ $], have I told you this one yet?",
    "Oh wait[ $,] I got a good story for you!",
    "I just remembered a great story[, $], hold on a sec."
]

ENDINGS = [
    "Anyway that's that story[ $], there you go.",
    "Isn't that hilarious? That one cracks me up every time, I tell you[, $].",
    "God damn I miss the old days[ $].",
    "Anyway see that[, $]? You young people ain't gonna have any stories as good as that!"
]

INTERRUPTED = [
    "Now hold on now, [$, ]I ain't done yet",
    "You can talk when I finish my story, damn it",
    "Do I interrupt you when you're talking?",
    "Young people are so damn rude nowadays. You let me finish my damn story",
    "Damn it[ $] I ain't finished!",
    "[$, ]Shut up and let me finish.",
    "I swear you interrupt me again I'll whoop you right here.",
    "Shut up, damnit!",
    "Damnit[ $], shut up and let me finish!"
]

FORGOT = [
    "Damn it I forgot what I was saying. I'll finish it later.",
    "I forget what happened next. Nevermind.",
    "Damn, I forgot the rest. Nevermind."
]

REMEMBER = [
    "Oh yeah,",
    "Anyway,",
    "Oh yeah,"
]

CAPS = [
    "Damn it not again, stupid fancy computers.",
    "Damn this stupid thing!",
    "I swear if that happens again I'm throwing this thing out.",
    "Damn it, give me a good typewriter any day."
]

CAPS_RETURNS = [
    "Give me a minute to figure out what I was sayin.[ Sorry about that $.]",
    "Give me a minute to sort my thoughts again.",
    "Hold on a sec [$ ]while I remember where I was."
]
