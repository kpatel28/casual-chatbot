#! /usr/bin/env python
#
# Example program using irc.bot.
#
# Joel Rosdahl <joel@rosdahl.net>
# slight modifications by Foaad Khosmood

"""A simple example bot.
This is an example bot that uses the SingleServerIRCBot class from
irc.bot.  The bot enters a channel and listens for commands in
private messages and channel traffic.  Commands in channel messages
are given by prefixing the text by the bot name followed by a colon.
It also responds to DCC CHAT invitations and echos data sent in such
sessions.
The known commands are:
    stats -- Prints some channel information.
    disconnect -- Disconnect the bot.  The bot will try to reconnect
                  after 60 seconds.
    die -- Let the bot cease to exist.
    dcc -- Let the bot invite you to a DCC CHAT connection.
"""

NICKNAME = 'cranky-abe-bot'

import irc.bot
import irc.strings
from irc.client import ip_numstr_to_quad, ip_quad_to_numstr
import conversation

class TestBot(irc.bot.SingleServerIRCBot):
    def __init__(self, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.channel = channel

    def on_nicknameinuse(self, c, e):
        c.nick(c.get_nickname() + "_")

    def on_welcome(self, c, e):
        c.join(self.channel)

    def on_privmsg(self, c, e):
        self.do_command(e, e.arguments[0])

    def on_pubmsg(self, c, e):
        a = e.arguments[0].split(":", 1)
        if len(a) > 1 and irc.strings.lower(a[0]) == irc.strings.lower(self.connection.get_nickname()):
            self.do_command(e, a[1].strip())
        return

    def on_dccmsg(self, c, e):
        # non-chat DCC messages are raw bytes; decode as text
        text = e.arguments[0].decode('utf-8')
        c.privmsg("You said: " + text)

    def on_dccchat(self, c, e):
        if len(e.arguments) != 2:
            return
        args = e.arguments[1].split()
        if len(args) == 4:
            try:
                address = ip_numstr_to_quad(args[2])
                port = int(args[3])
            except ValueError:
                return
            self.dcc_connect(address, port)

    def do_command(self, e, cmd):
        nick = e.source.nick
        c = self.connection

        if cmd == "disconnect":
            self.disconnect()
        elif cmd == "die":
            c.privmsg(self.channel, "Hey! Screw you, punk!")
            #self.die()
        elif cmd == "stats":
            for chname, chobj in self.channels.items():
                c.notice(nick, "--- Channel statistics ---")
                c.notice(nick, "Channel: " + chname)
                users = sorted(chobj.users())
                c.notice(nick, "Users: " + ", ".join(users))
                opers = sorted(chobj.opers())
                c.notice(nick, "Opers: " + ", ".join(opers))
                voiced = sorted(chobj.voiced())
                c.notice(nick, "Voiced: " + ", ".join(voiced))
        elif cmd == "dcc":
            dcc = self.dcc_listen()
            c.ctcp("DCC", nick, "CHAT chat %s %d" % (
                ip_quad_to_numstr(dcc.localaddress),
                dcc.localport))
        elif cmd == "about":
            c.privmsg(self.channel, "")
        elif cmd == "forget":
            c.privmsg(self.channel, "Woah, I don't feel so good...")
            conversation.forget()
        elif cmd == "usage":
            #Foaad: change this
            c.privmsg(self.channel, "I can answer questions like this:") 
            c.privmsg(self.channel, "    First you need to handle the greeting:")
            c.privmsg(self.channel, "    hi|how are you?|I'm fine, etc.")
            c.privmsg(self.channel, "    Then you can ask some simple questions about me like my age.")
            c.privmsg(self.channel, "    Mostly though I like to tell stories.")
            c.privmsg(self.channel, "    You can ask me to tell you one, or wait long enough and I'll bring one up myself.")
            c.privmsg(self.channel, "    Or you can mention something that reminds me of a particular story (there's a lot to mention but these will gurantee you all the unique stories I have: 'cow', 'mayo', 'elmo', 'bull', 'typewriter', 'jail')")
            c.privmsg(self.channel, "Have fun! And watch your damn language.") 
        else:
            # actual conversations
            conversation.takeInput(cmd, False, c, self.channel, nick)
def main():
    import sys
    server = 'irc.freenode.net'
    port = 6667
    channel = sys.argv[1]
    nickname = 'Cranky-Abe'
    print("channel:", channel)
    bot = TestBot(channel, nickname, server, port)
    bot.start()


if __name__ == "__main__":
    main()
