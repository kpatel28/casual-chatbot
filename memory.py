import random

MEMORY = []

RECALL_KEYS = ['i say', '?']

def forget():
    global MEMORY
    MEMORY = []

def getScore(inp, mem):
    score = 0
    for word1 in mem.lower().split():
        for word2 in inp.lower().split():
            if word1 == word2:
                score += 1
    return score

def getResponse(found):
    replaces = {
        'my' : 'your',
        'i' : 'you'
    }
    if found[-1] in '.,?!"':
        found = found[:-1]
    
    found = found.replace('I am', 'you are')
    found = found.replace('i am', 'you are')
    found = found.replace('I was', 'you were')
    found = found.replace('i was', 'you were')
    
    found = found[0].lower() + found[1:]
    parts = found.split()
    for i in range(len(parts)):
        w = parts[i]
        if w in replaces:
            parts[i] = replaces[w]
            
    intros = [
        "Your memory's worse than mine[, $]!",
        "You get hit in the head or something[, $]?",
        "You not payin attention[, $]?",
        "[$, ]I thought young folk were supposed to have good memory."
    ]
    return random.choice(intros) + " I'm pretty sure you said " + ' '.join(parts) + '.'

def tieBreak(results, inp):
    scores = {}
    for result in results:
        score = 0
        for word in result.lower().split():
            if word in inp.lower().split():
                score += 1
            else:
                score -= 1
        scores[result] = score
    return max(scores, key=scores.get)

def recall(inp):
    if len(MEMORY) == 0:
        return None
    # check if requesting recall
    rec = True
    for key in RECALL_KEYS:
        if key not in inp.lower():
            rec = False
    if not rec:
        return None
    # perform recall
    scores = {}
    for mem in MEMORY:
        if 'my' in mem.lower().split() or 'i' in mem.lower().split():
            scores[mem] = getScore(inp, mem)
        pass
    
    # find best scores
    max_value = max(scores.values())
    result = [key for key, value in scores.items() if value == max_value]
    
    if len(result) > 0:
        best = tieBreak(result, inp)
    else:
        best = result[0]
    if scores[best] == 0:
        return "My memory ain't wait it used to be[, $]."
    else:
        return getResponse(best)

def remember(inp):
    if ('i' in inp.lower().split() or 'my' in inp.lower().split()) and '?' not in inp:
        MEMORY.append(inp)
        return True
    return False
